# [Computational Graphic Design Manual](https://dpmanual.bitbucket.io/)

{ graphics manual for the algorithmic age }

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---


## Introduction

There is a firm tradition in the visual arts that draws fundamental learnings from the theory of point, line and surface. Many of these concepts have found their place in graphic design theory and practice, helping the artist to create with a firm yet flexible set of general rules that underlie the finer details of graphic composition, form, combination and variation. This is an on-going collection of programs written in Processing & P5js, introducing a variety of these abstract visual concepts and hence extending them to the realm of data, data structures and algorithmic thinking.

## Contents
I'll be adding source files as the project progresses.

* DP_Processing - Processing sketches
* DP_P5js - JavaScript(P5js) sketches
* DP_OPENRNDR

## Website

This repository is accompanied by a website : [Computational Graphic Design Manual](https://dpmanual.bitbucket.io/)

## Install

The programs in this repository can be compiled using Processing, OPENRNDR or with P5js in a browser.

* [https://processing.org/](https://processing.org/)
* [https://p5js.org/](https://p5js.org/)
* [https://openrndr.org/](https://openrndr.org/)

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.05
* Tools used : Processing, P5js & OPENRNDR

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution. You can equally contact me directly via email. 

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
