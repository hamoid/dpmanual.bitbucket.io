/**
 * 
 */
 

let radius = 50;
let sw = 14;

/////////////////////////// SETUP ////////////////////////////

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noFill();
  stroke(255);
  
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
    background(0);
    strokeWeight(sw);
    let angle = map(mouseY, 0, height, 0, 360);
    
    let num = map(mouseX, 0, width, 1, 60);
    for(let i=1; i<num; i++){
      drawForm(width/2, height/2, 3, radius*i, angle*(i*0.05));
    }
}

/////////////////////////// FUNCTIONS ////////////////////////////
function drawForm( _x,  _y,  _num,  _rad,  _angle) {
  push();
  translate(_x, _y);
  rotate(radians(_angle));
  let angle = radians(360/_num); 
  beginShape();
  for (let i = 0; i<_num; i++) {

    let x = cos(i * angle) * _rad;
    let y = sin(i * angle) * _rad;
    
    vertex(x, y);
  }
  endShape(CLOSE);
  pop();
}


function keyPressed() {
  //if (key == 's') saveFrame("savedImage_###.png");
  if(key == '+'){
    radius+=5;
  }
   if(key == '-'){
    radius-=5;
  }
  if (key == 'w') sw +=2;
  if (key == 'x') {
    if(sw>0){
    sw -=2;
    }
  }
}