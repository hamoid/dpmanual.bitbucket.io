/**
 * Random Points
 * 
 */


let randSeed = 1;
/////////////////////////// SETUP ////////////////////////////

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  background(0);
  stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  randomSeed(randSeed);
  let numPoints = map(mouseX, 0, width, 1, 500);
  let cutOffVal = map(mouseY, 0, height, 1, 125);
  for(let i=0; i<numPoints; i++){
    let pntSize = random(1, cutOffVal);
    strokeWeight(pntSize);
    let x = random(width);
    let y = random(height);
    point(x, y);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
function keyPressed() {
    if (key == 'r') {
    randSeed = random(1000);
  }
}