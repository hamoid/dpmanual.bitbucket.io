/*
 * Sketch: DPMTexture_003
 * Parent Sketch: none
 * Type: static
 *
 * Summary : Repeating modules 
 * REF : https://twitter.com/paulrickards/status/1150519293215465475/photo/3
 *
 * GIT:
 * Author: mark webster 2019
 * https://dpmanual.bitbucket.io
 * https://designingprograms.bitbucket.io
 */

let tileSize = 50;
let angle;
let sw = 1.0;
let theCol;
let moduleNum = 1;
let randSeed;

let isRotate = true;
let isRandomTile, isOutline = false;

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    background(0);
    smooth(8);
    rectMode(CENTER);
    strokeCap(SQUARE);
    randSeed = random(1000);
    theCol = color(255);
}


function draw() {
    background(0);
    randomSeed(randSeed);
    strokeWeight(sw);
    for (let y = tileSize / 2; y < height; y += tileSize) {
        for (let x = tileSize / 2; x < width; x += tileSize) {
            let rndAngle = Math.floor(random(5));
            if (rndAngle == 0) {
                angle = 0;
            }
            if (rndAngle == 1) {
                angle = 90;
            }
            if (rndAngle == 2) {
                angle = 180;
            }
            if (rndAngle === 3) {
                angle = 270;
            }
            
            push();
            translate(x, y);

            if (isRotate) {
                rotate(radians(angle));
            }
            if (isRandomTile) {
                let rndGen = Math.floor(random(9));
                chooseModule(0, 0, tileSize, isOutline, rndGen);
            } else {
                chooseModule(0, 0, tileSize, isOutline, moduleNum);
            }
            pop();
        }
    }
}

function keyPressed() {
    if(key === 'r') {randSeed = random(1000);}
    if (key == 't') {
        isRandomTile = !isRandomTile;
    }
    if (key == 'a') {
        isRotate = !isRotate;
    }
    if (key == 'o') {
        isOutline = !isOutline;
    }
    if (key == 'l') {
        tileSize += 25;
    }
    if (key == 'm') {
        if (tileSize > 50) {
            tileSize -= 25;
        }
    }

    if (key == 'w') {
        sw += 0.5;
    }
    if (key == 'x') {
        if (sw > 0.5) {
            sw -= 0.5;
        }
    }
    if (key == '1') {
        moduleNum = 1;
    }
    if (key == '2') {
        moduleNum = 2;
    }
    if (key == '3') {
        moduleNum = 3;
    }
    if (key == '4') {
        moduleNum = 4;
    }
    if (key == '5') {
        moduleNum = 6;
    }
    if (key == '6') {
        moduleNum = 7;
    }
}

/////////////////////////////////// 
/**
 *
 * Various tiles based on Edward Zajec's early computer art
 * REF : http://www.edwardzajec.com/tvc4/ser/index.html
 * REF : https://monoskop.org/Edward_Zajec
 */


function module_01(x, y, taille, isOutline) {
    push();
    if (isOutline) {
        stroke(theCol);
    } else {
        noStroke();
    }
    noFill();
    translate(x, y);
    rect(0, 0, taille, taille);
    pop();
}

function module_02(x, y, taille, isOutline) {
    push();
    translate(-taille / 2, -taille / 2);
    stroke(theCol);
    if (isOutline) {
        noFill();
        rect(x + taille / 2, y + taille / 2, taille, taille);
    } else {}

    translate(x, y);
    line(0, taille / 2, taille / 2, 0);
    line(0, taille, taille, 0);
    let inter = taille / 10;
    let xStart = taille;
    for (let j = 0; j < taille; j += inter) {
        line(xStart, j, taille, j);
        xStart -= inter;
    }
    pop();
}

function module_03(x, y, taille, isOutline) {
    push();
    translate(-taille / 2, -taille / 2);
    stroke(theCol);
    if (isOutline) {
        noFill();
        rect(x + taille / 2, y + taille / 2, taille, taille);
    } else {}

    fill(theCol);
    translate(x, y);
    strokeWeight(sw + 0.5);
    triangle(0, taille, taille, taille, taille, 0);
    let inter = taille / 10;
    let xStart = taille;
    strokeWeight(sw);
    for (let j = 0; j <= taille; j += inter) {
        line(0, j, xStart, j);
        xStart -= inter;
    }
    pop();
}

function module_04(x, y, taille, isOutline) {
    push();
    translate(-taille / 2, -taille / 2);
    stroke(theCol);
    if (isOutline) {
        noFill();
        rect(x + taille / 2, y + taille / 2, taille, taille);
    } else {}

    fill(theCol);
    translate(x, y);
    triangle(0, 0, taille, 0, 0, taille);
    line(0, taille, taille, 0);
    line(taille / 2, taille, taille, taille / 2);
    pop();
}

function module_05(x, y, taille, isOutline) {
    push();
    translate(-taille / 2, -taille / 2);
    stroke(theCol);
    if (isOutline) {
        noFill();
        rect(x + taille / 2, y + taille / 2, taille, taille);
    } else {}

    noFill();
    translate(x, y);
    let inter = taille / 10;
    for (let j = 0; j <= taille; j += inter) {
        line(0, j, taille, j);
    }

    pop();
}

function module_06(x, y, taille, isStroke) {
    push();
    if (isStroke) {
        stroke(theCol);
    } else {
        noStroke();
    }
    fill(theCol);
    translate(x, y);
    rect(0, 0, taille, taille);
    pop();
}

function module_07(x, y, taille, isOutline) {
    push();
    translate(-taille / 2, -taille / 2);
    stroke(theCol);
    if (isOutline) {
        noFill();
        rect(x + taille / 2, y + taille / 2, taille, taille);
    } else {}

    noFill();
    translate(x, y);
    line(0, taille / 2, taille / 2, 0);
    line(0, taille, taille, 0);
    line(taille / 2, taille, taille, taille / 2);
    pop();
}

function module_08(x, y, taille, isOutline) {
    push();
    translate(-taille / 2, -taille / 2);
    stroke(theCol);
    if (isOutline) {
        noFill();
        rect(x + taille / 2, y + taille / 2, taille, taille);
    } else {}
    fill(theCol);
    strokeWeight(sw + 0.5);
    translate(x, y);
    triangle(0, 0, taille, 0, 0, taille);
    let inter = taille / 10;
    let xStart = taille;
    //stroke(255);
    strokeWeight(sw);
    for (let j = 0; j <= taille; j += inter) {
        line(xStart, j, taille, j);
        xStart -= inter;
    }
    pop();
}

/**
 * A function that draws modules
 * based on a number input from 0 > n (modules)
 */
function chooseModule(x, y, t, isOn, rndNum) {

    if (rndNum == 0) {
        module_01(x, y, t, isOn);
    }
    if (rndNum == 1) {
        module_02(x, y, t, isOn);
    }
    if (rndNum == 2) {
        module_03(x, y, t, isOn);
    }
    if (rndNum == 3) {
        module_04(x, y, t, isOn);
    }
    if (rndNum == 4) {
        module_05(x, y, t, isOn);
    }
    if (rndNum == 5) {
        module_06(x, y, t, isOn);
    }
    if (rndNum == 6) {
        module_07(x, y, t, isOn);
    }
    if (rndNum == 7) {
        module_08(x, y, t, isOn);
    }
}