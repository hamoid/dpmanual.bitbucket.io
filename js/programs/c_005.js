/**
 * Circle: Arcs
 * 
 * Sketch : DPMArc_001_p5js
 */


let step = 1;

function setup() {
let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noStroke();
  fill(255);
}

function draw() {
    background(0);   
  let num = map(mouseX, 0, width*2, 0, 500);
  let size = map(mouseY, 0, height, 1, 50); 
  
  translate(width/2, height/2);
  for (let i = 0; i< num; i++) {
    let x = cos(i * step) * i;
    let y = sin(i * step) * i;
    ellipse(x, y, size, size);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////

function keyPressed() {
    if (key == '+') step++;
  if (key == '-') {
    if (step>1)
      step--;
  }

}