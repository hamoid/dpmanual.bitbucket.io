/**
 * Repeating Circle
 * 
 */


let isProgressive = false;
let dia;
/////////////////////////// SETUP ////////////////////////////

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    background(0);
    noFill();
    stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
    background(0);
    let sw = map(mouseY, 0, height, 1, 25);
    strokeWeight(sw);
    let num = map(mouseX, 0, width, 20, 5);
    drawForm(width/2, height/2, num);
}

/////////////////////////// FUNCTIONS ////////////////////////////
function drawForm( _x,  _y,  _num) {
    push();
    translate(_x, _y);
    if(!isProgressive){
      dia = _num*15;
    }
    for (let i = 0; i< 40; i++) {
      if(isProgressive){
         dia = _num*i;
      }
      ellipse(0, 0, dia*i, dia*i);
    }
    pop();
  }

  function keyPressed(){
    if(key ==='p'){
      isProgressive =!isProgressive; 
    }
  }