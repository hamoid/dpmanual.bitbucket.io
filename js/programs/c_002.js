/**
 * Repeating Square
 * 
 */

let num = 7;
let isAlternate = false;
/////////////////////////// SETUP ////////////////////////////

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    background(0);
    noFill();
    stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
    background(0);
    let sw = map(mouseY, 0, height, 1, 50);
    strokeWeight(sw);
    let angle = map(mouseX, 0, width, 0, 0.35);
    drawForm(width/2, height/2, angle, num);
}

/////////////////////////// FUNCTIONS ////////////////////////////
function drawForm( _x,  _y,  _a, _num) {
    push();
    translate(_x, _y);
    rectMode(CENTER);
    let shpSize = _num*15;
    let a = 0;
    for (let i = 0; i< width; i++) {
      if(isAlternate){
        if(i%2==0){
          rotate(radians(a*i));
        }else{
          rotate(radians(-a*i));
        }
      }else {
        rotate(radians(a*i));
      }
      rect(0, 0, 10+shpSize*i, 10+shpSize*i);
      a+=_a;
    }
    pop();
  }


  function keyPressed() {
    if(key == '+') {num++;}
    if(key == '-') {num--;}
    if(key == 'a'){isAlternate =!isAlternate;}
  }