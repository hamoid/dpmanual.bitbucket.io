/**
 * A grid of lines
 * 
 */



let gridStep = 55; // gap between each
let lineLen = 50; // length of lines
let lineDensity = 10; // number of repetitions
let strokeWidth = 1.5;
let randSeed;
/////////////////////////// SETUP ////////////////////////////

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  background(0);
  smooth();
  stroke(255);
  strokeCap(SQUARE);
  randSeed = random(1000);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  randomSeed(randSeed);
  strokeWeight(strokeWidth);
  for (let y=0; y<=height; y+=gridStep) {
    for (let x=0; x<=width; x+=gridStep) {
    let randVal = random(12);        // parse to Int ?
    let cutOffValX = map(mouseX, 0, width, 0, 12);
    let cutOffValY = map(mouseY, 0, height, 0, 12);
      if (randVal <= cutOffValX) {
        drawLines(x, y, lineLen, true);
      }
      if (randVal > cutOffValY) {
        drawLines(x, y, lineLen, false);
      }
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
function drawLines( _x,  _y,  _len,  _isVertical) {
  let inter = gridStep/lineDensity;
  for (let i=0; i<gridStep; i+=inter) {
    if(_isVertical){
      line(_x+i, _y-_len, _x+i, _y+_len);
    }else {
      line(_x-_len, _y+i, _x+_len, _y+i);  
    }
  }
}

function keyPressed() {
    if(key === 'r') {randSeed = random(1000);}
    if(key === '+') {gridStep+=5; lineLen+=5;}
    if(key === '-') if(gridStep>5){gridStep-=5;  lineLen-=5;}
    if(key === 'w') lineLen +=5;
    if(key === 'x') if(lineLen>0){lineLen -=5;}
    if(key === 'l') lineDensity +=1;
    if(key === 'm') if(lineDensity>0){lineDensity -=1;}
}
