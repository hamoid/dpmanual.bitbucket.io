/**
 * 
 * A simple point in space
 */


function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    noStroke();
}

function draw() {
    background('black');
    stroke(255);
    let pntSize = map(sin(frameCount * 0.012), -1, 1, 0, 600);
    strokeWeight(50 + pntSize);
    point(250, 250);
}