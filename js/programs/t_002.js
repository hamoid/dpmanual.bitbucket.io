/**
 * Pattern
 */


let gridStep = 100; // gap between each
let lineLen = gridStep; // length of lines
let sw = 2;
let interval = 10;
let randSeed;
/////////////////////////// SETUP ////////////////////////////

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  background(0);
  smooth();
  stroke(255);
  strokeCap(SQUARE);
  randSeed = random(1000);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  randomSeed(randSeed);
  strokeWeight(sw);
  for (let y=0; y<=height-30; y+=gridStep) {
    for (let x=0; x<=width-30; x+=gridStep) {
        form(x, y, lineLen, 0);
        form(x-10, y,lineLen, 1);
        form(x, y, lineLen, 2);
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
function form( _x,  _y,  _size,  _type) {
    stroke(random(255));
    push();
    translate(_x, _y);
    for(let i = 0; i < _size; i+=interval) {
        let j = _size-i;
        
        if(_type==0){
          line(i, 0, _size, j);
        }
          else if(_type==1){
          line(10, i, j+10, _size);
        }
          else if(_type==2){
          line(i, _size,_size, i);
        }
    }  
    pop();
  }

function keyPressed() {
    if(key === 'r') {randSeed = random(1000);}
    if(key === '+') {interval++;}
    if(key === '-') if(interval>5){interval--;}
    if(key === 'w') sw ++;
    if(key === 'x') if(sw>0){sw--;}
    if(key === 'l') {gridStep +=25; lineLen = gridStep;}
    if(key === 'm') if(gridStep>50){gridStep -=25; lineLen = gridStep;}
}