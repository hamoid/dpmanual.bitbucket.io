/**
 * HARMONIC MOTION
 * 
 * Sketch : DPM_004_p5js
 */


let size = 5;
let isAlternate = false;
let cnv;
function setup() {
   cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noStroke();
}

function draw() {
    background(0);
    fill(255);
    let num = map(mouseX, 0, cnv.width*2, 0, 100);

  for (let i=0; i<num; i++) {
    let x = map(i, 0.0, num, 10.0, width);
    let waveHeight = map(mouseY, 0.0, height, -height/2.0, height/2.0);
    let delay = map(i, 0.0, num, -1.0, 1.0);
    let delay2 = 0;
    for (let j = 0; j< num; j++) {
        let y = map(j, 0.0, num, 10.0, height);
      if (isAlternate) {
        delay2 = delay + map(j, 0.0, num, -1.0, 1.0);
      } else {
         delay2 = map(i, 0.0, num, -1.0, 1.0);
      }
      ellipse(x, y + waveHeight * sin(delay2 + frameCount*0.015), size, size);
    }
  }
}

function keyPressed() {
  if (key == '+') {
    size++;
  }
  if (key == '-') {
    if (size > 1) {
        size--;
    }
  }

  if(key == 'a'){
    isAlternate = !isAlternate;
  }
}