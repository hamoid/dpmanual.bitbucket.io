package Motion

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.math.map
import org.openrndr.shape.Circle
import java.lang.Math.sin

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var size = 5.0
        var isAlternate = false

        keyboard.character.listen {
            if (it.character == 'a') { isAlternate = !isAlternate }
            if (it.character == '+') { size++ }
            if (it.character == '-') { if(size>2) { size-- } }
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.stroke = null
            drawer.fill = ColorRGBa.PINK

            val num = map(0.0, width.toDouble(), 1.0, 150.0, mouse.position.x).toInt()

            for (i in 1 until num) {
                val x = map(0.0, num.toDouble(), 10.0, width - 10.0, i.toDouble())
                val waveHeight = map(0.0, height.toDouble(), -(height/2.0), height/2.0, mouse.position.y)
                val delay = map(0.0, num.toDouble(), -1.0, 1.0, i.toDouble())

                val c = mutableListOf<Circle>()
                for (j in 1 until num) {
                    val y = map(0.0, num.toDouble(), 10.0, height - 10.0, j.toDouble())

                    val delay2 = if(isAlternate) {
                        delay + map(0.0, num.toDouble(), -1.0, 1.0, j.toDouble())
                    } else {
                        delay
                    }

                    c.add(Circle(x, y + (waveHeight * sin(delay2 + seconds)), size))
                }

                drawer.circles(c)
            }
        }
    }
}