package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.ffmpeg.ScreenRecorder
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var thickness = 1.0

        extend(ScreenRecorder())
        extend(Screenshots())

        keyboard.character.listen {
            if (it.character == '+') { thickness++ }
            if (it.character == '-') { thickness-- }
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.fill = null
            drawer.stroke = ColorRGBa.PINK
            drawer.strokeWeight = thickness

            val num = map(0.0, width.toDouble(), 0.0, 150.0, mouse.position.x).coerceIn(0.0, 150.0).toInt()
            val size = map(0.0, height.toDouble(), 0.0, 250.0, mouse.position.y).coerceIn(0.0, 250.0)

            drawer.translate(width/2.0, height/2.0)
            for (i in 0 until num) {
                drawer.isolated {
                    val degree = map(0.0, num.toDouble(), 0.0, 360.0, i.toDouble())
                    drawer.rotate(degree)
                    drawer.lineSegment(0.0, size, width/1.0, size)
                }
            }
        }
    }
}