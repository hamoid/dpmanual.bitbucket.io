package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.math.map
import kotlin.math.sin

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {
        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null

            val pntSize = map(-1.0, 1.0, 25.0, 300.0, sin(seconds))
            drawer.circle(width/2.0, height/2.0, pntSize)
        }
    }
}