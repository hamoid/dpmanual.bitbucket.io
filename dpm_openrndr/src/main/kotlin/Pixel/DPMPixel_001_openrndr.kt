package Pixel

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extra.noise.valueLinear
import org.openrndr.math.map
import org.openrndr.shape.Rectangle

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var randSeed = 1

        keyboard.character.listen {
            if (it.character == 'r') { randSeed = (Math.random() * 1000).toInt() }
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null

            if (mouse.position.x > 0.0 && mouse.position.y > 0.0) {
                val pixelDensity = map(0.0, height.toDouble(), 1.0, 73.0, mouse.position.y).coerceIn(1.0, 73.0)

                val colums = width/pixelDensity.toInt()
                val rows = height/pixelDensity.toInt()

                val pixels = mutableListOf<Rectangle>()
                for (i in 0 until colums) {
                    for (j in 0 until rows) {
                        val randVal = valueLinear(randSeed, i.toDouble(), j.toDouble()) * 12.0 + 12.0
                        val cutOffVal = map(0.0, width.toDouble(), 1.0, 12.0, mouse.position.x).coerceIn(1.0, 12.0)
                        if (randVal > cutOffVal) {
                            pixels.add(Rectangle(i*pixelDensity, j*pixelDensity, pixelDensity+1.0, pixelDensity+1.0))
                        }
                    }
                }
                drawer.rectangles(pixels)
            }
        }
    }
}