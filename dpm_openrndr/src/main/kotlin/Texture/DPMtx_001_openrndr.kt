package Texture

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.math.map
import org.openrndr.shape.Circle

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var interval = 10

        keyboard.character.listen {
            if (it.character == '+') { interval++ }
            if (it.character == '-') { if(interval>1){ interval-- } }
        }

        fun drawGrid(d: Double) {
            val circles = mutableListOf<Circle>()
            for (y in -height/2*2 until height-50 step interval) {
                for (x in -width/2*2 until width-50 step interval) {
                    circles.add(Circle(x.toDouble(), y.toDouble(), d))
                }
            }
            drawer.circles(circles)
        }

        extend {
            drawer.background(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null
            drawer.translate(width/2.0, height/2.0)

            val dia2 = map(0.0, height.toDouble(), 1.0, 10.0, mouse.position.y)
            drawGrid(dia2)
            drawer.rotate(mouse.position.x*0.05)
            drawGrid(dia2)
        }
    }
}