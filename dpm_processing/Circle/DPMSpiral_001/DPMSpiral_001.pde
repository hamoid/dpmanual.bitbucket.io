/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

int step = 1;

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(500, 500);
  noStroke();
  fill(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);

  int num = (int)map(mouseX, 0, width, 0, 500);
  float size = map(mouseY, 0, height, 1, 50); 
  
  translate(width/2, height/2);
  for (int i = 0; i< num; i++) {
    float x = cos(i * step) * i;
    float y = sin(i * step) * i;
    ellipse(x, y, size, size);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
  if (key == '+') step++;
  if (key == '-') {
    if (step>1)
      step--;
  }
}