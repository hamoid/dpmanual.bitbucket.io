/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


/////////////////////////// GLOBALS ////////////////////////////
int tileSize = 50;
int angle = 0;
float sw = 1.0;
color theCol;
int moduleNum = 1;
int seed;

boolean isRotate = true;
boolean isRandomTile, isOutline = false;

void setup() {
  size(500, 500);
  background(0);
  smooth(8);
  rectMode(CENTER);
  strokeCap(SQUARE);
  seed = (int)random(1000);
  theCol = color(255);
}


void draw() {
  background(0);
  randomSeed(seed);
  strokeWeight(sw);
  for (int y=tileSize/2; y<height; y+=tileSize) {
    for (int x=tileSize/2; x<width; x+=tileSize) {
      int rndAngle = (int)random(5);
      if (rndAngle==0) {
        angle = 0;
      }
      if (rndAngle==1) {
        angle = 90;
      }
      if (rndAngle==2) {
        angle = 180;
      }
      if (rndAngle==3) {
        angle = 270;
      }
      //////////////////// > RANDOM GEN

      pushMatrix();
      translate(x, y);
      if (isRotate) {
        rotate(radians(angle));
      }
      if (isRandomTile) {
        int rndGen = (int)random(9);
        chooseModule(0, 0, tileSize, isOutline, rndGen);
      } else {
        chooseModule(0, 0, tileSize, isOutline, moduleNum);
      }
      popMatrix();
    }
  }
}

void keyPressed() {
  if (key == 'r') {
    seed = (int)random(1000);
  }

  if (key == 's') {
    saveFrame("export_###.png");
  }
  if (key == 't') {
    isRandomTile = !isRandomTile;
  }
  if (key == 'a') {
    isRotate = !isRotate;
  }
  if (key == 'o') {
    isOutline = !isOutline;
  }
  if (key == 'l') {
    tileSize+=25;
  }
  if (key == 'm') {
    if (tileSize>50) {
      tileSize-=25;
    }
  }

  if (key == 'w') {
    sw+=0.5;
  }
  if (key == 'x') {
    if (sw>0.5) {
      sw-=0.5;
    }
  }
  if (key == '1') {
    moduleNum = 1;
  }
  if (key == '2') {
    moduleNum = 2;
  }
  if (key == '3') {
    moduleNum = 3;
  }
  if (key == '4') {
    moduleNum = 4;
  }
  if (key == '5') {
    moduleNum = 6;
  }
  if (key == '6') {
    moduleNum = 7;
  }
}