/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
int colums, rows;
int randSeed = 1;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  smooth();
  noStroke();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  randomSeed(randSeed);
  int pixelDensity = (int)map(mouseY, 0, height, 1, 100);
  drawPixels(pixelDensity);
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawPixels(int _density) {
  colums = width/_density;
  rows = height/_density;
  for (int i=0; i<colums; i++) {
    for (int j=0; j<rows; j++) {
      float randVal = random(12);
      float cutOff = map(mouseX, 0, width, 0, 12);
      if (randVal > cutOff) {
        rect( i*_density, j*_density, _density, _density);
      }
    }
  }
}

void keyPressed() {
  if (key == 'r') {
    randSeed = (int)random(1000);
  }
  if (key == 's') saveFrame("savedImage_###.png");
}