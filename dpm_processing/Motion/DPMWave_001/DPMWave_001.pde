/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

int size = 5;
boolean isAlternate = false;

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(500, 500);
  noStroke();
  fill(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);

  int num = (int)map(mouseX, 0, width, 0, 150);

  for (int i=0; i<num; i++) {
    float x = map(i, 0.0, num, 10.0, width);
    float waveHeight = map(mouseY, 0.0, height, -height/2.0, height/2.0);
    float delay = map(i, 0.0, num, -1.0, 1.0);
    float delay2 = 0;
    for (int j = 0; j< num; j++) {
      float y = map(j, 0.0, num, 10.0, height);
      if (isAlternate) {
        delay2 = delay + map(j, 0.0, num, -1.0, 1.0);
      } else {
         delay2 = map(i, 0.0, num, -1.0, 1.0);
      }
      ellipse(x, y + waveHeight * sin(delay2 + frameCount*0.015), size, size);
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
  if (key == '+') size++;
  if (key == '-') {
    if (size>1)
      size--;
  }
  if (key == 'a') {
    isAlternate = !isAlternate;
  }
}