/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

float thickness = 35;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  smooth();
  strokeCap(SQUARE);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  stroke(255);
  float angle = map(mouseX, 0, width, 1, 720);
  float len = map(mouseY, 0, height, 1, 600);
  strokeWeight(thickness);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(angle));
  line(-len, -len, len, len);
  popMatrix();
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key =='s') {
    saveFrame("capture_###.png");
  }
  if (key =='+') {
    thickness+=5;
  }
  if (key =='-') {
    if(thickness>5){
      thickness-=5;
    }
  }
}