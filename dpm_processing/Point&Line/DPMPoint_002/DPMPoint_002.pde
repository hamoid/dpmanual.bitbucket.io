/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


int gridStep = 50;
int pntSize = 50;
int randSeed = 1;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  randomSeed(randSeed);
  stroke(255);
  strokeWeight(pntSize);
  for (int y=pntSize; y<=height-pntSize; y+=gridStep) {
    for (int x=pntSize; x<=width-pntSize; x+=gridStep) {
      float randVal = random(12);
      float cutOffVal = map(mouseY, 0, height, 0, 12);
      if (randVal>cutOffVal) {
        point(x, y);
      }
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 's') saveFrame("savedImage_###.png");
  if (key == 'r') {
    randSeed = (int)random(1000);
  }
  if (key == '+') {
    gridStep+=5; 
    pntSize+=5;
  }
  if (key == '-') if (gridStep>5) {
    gridStep-=5;  
    pntSize-=5;
  }
  if (key == 'w') pntSize +=5;
  if (key == 'x') pntSize -=5;
}