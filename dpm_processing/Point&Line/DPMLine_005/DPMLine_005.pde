/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////

int randSeed;
int lineLength = 10;
int sw = 30;
boolean isRandAngle = false;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  stroke(255);
  strokeCap(SQUARE);
  randSeed = (int)random(5000);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  randomSeed(randSeed);
  strokeWeight(sw);
  float angle = 0;
  int numLines = (int)map(mouseX, 0, width, 1, 150);
  float a = map(mouseY, 0, height, 0, 360);
  for (int i=0; i<numLines; i++) {
    if (isRandAngle) {
      angle = random(a);
    } else {
      angle = 0;
    }
    float randLen = random(10, 200);

    float x = random(width);
    float y = random(height);
    pushMatrix();
    translate(x, y);
    rotate(radians(angle+a));
    line(-randLen-lineLength, 0, randLen+lineLength, 0);
    popMatrix();
  }
}


void keyPressed() {
  if (key == 's') {
    saveFrame("export_DPMOsc_002_###.png");
  }
  if (key == 'r') {
    randSeed = (int)random(5000);
  }
  if (key == 'a') {
    isRandAngle = !isRandAngle;
  }
  if (key == '+') {
    sw+=5;
  }
  if (key == '-') {
    if (sw>0)
      sw-=5;
  }
  if (key == 'l') {
    lineLength+=5;
  }
  if (key == 'm') {
    lineLength-=5;
  }
}