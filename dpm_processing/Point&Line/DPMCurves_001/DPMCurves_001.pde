/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(500, 500);
  noFill();
  stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  int num = (int)map(mouseX, 0, width, 1, 75);
  int thickness = (int)map(mouseY, 0, height, 1, 5);          
  strokeWeight(thickness);
  for (int i = 0; i< num; i++) {
    float linePositionX = map(i, 0.0, num, 10.0, width - 10.0);
    bezier(linePositionX, 0, mouseX, mouseY, mouseX, mouseY, linePositionX, height);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
}