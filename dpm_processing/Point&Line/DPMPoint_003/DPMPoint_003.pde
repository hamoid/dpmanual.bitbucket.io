/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


int randSeed = 1;
int numPoints = 500;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  randomSeed(randSeed);
  numPoints = (int)map(mouseX, 0, width, 1, 5000);
  float cutOffVal = map(mouseY, 0, height, 1, 50);
  for(int i=0; i<numPoints; i++){
    float pntSize = random(1, cutOffVal);
    strokeWeight(pntSize);
    float x = random(width);
    float y = random(height);
    point(x, y);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////


void keyPressed() {
  if (key == 's') saveFrame("savedImage_###.png");
    if (key == 'r') {
    randSeed = (int)random(1000);
  }
}